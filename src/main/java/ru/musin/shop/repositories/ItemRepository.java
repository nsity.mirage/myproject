package ru.musin.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.musin.shop.model.Item;
import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item, Long> {
    void deleteItemById(Long id);

    Optional<Item> findItemById(Long id);
}
