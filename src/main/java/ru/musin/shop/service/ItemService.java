package ru.musin.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.musin.shop.exception.ItemNotFoundException;
import ru.musin.shop.model.Item;
import ru.musin.shop.repositories.ItemRepository;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class ItemService {
    private final ItemRepository itemRepo;

    @Autowired
    public ItemService(ItemRepository itemRepo) {
        this.itemRepo = itemRepo;
    }

    public Item addItem(Item item) {
        item.setPersonCode(UUID.randomUUID().toString());
        return itemRepo.save(item);
    }

    public List<Item> findAllItem() {
        return itemRepo.findAll();
    }

    public Item updateItem(Item item) {
        return itemRepo.save(item);
    }

    public Item findItemById(Long id) {
        return itemRepo.findItemById(id)
                .orElseThrow(() -> new ItemNotFoundException("Item by id " + id + " was not found"));
    }

    public void deleteItem(Long id){
        itemRepo.deleteItemById(id);
    }
}
