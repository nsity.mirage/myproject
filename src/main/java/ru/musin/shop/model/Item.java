package ru.musin.shop.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Item implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private String category;
    private String description;
    private String price;
    private String imageUrl;
    @Column(nullable = false, updatable = false)
    private String personCode;

    public Item() {}

    public Item(String name, String category, String description, String price, String imageUrl, String personCode) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
        this.personCode = personCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String email) {
        this.category = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String jobTitle) {
        this.description = jobTitle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String phone) {
        this.price = phone;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPersonCode() {
        return personCode;
    }

    public void setPersonCode(String employeeCode) {
        this.personCode = employeeCode;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
